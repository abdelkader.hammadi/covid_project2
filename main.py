#Import required libraries
import pickle
import copy
import pathlib
import pandas as pd
import plotly          
import plotly.express as px
import dash            
import dash_table
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
from pymongo import MongoClient
import plotly.graph_objects as go



app = dash.Dash(__name__)
server = app.server
app.config.suppress_callback_exceptions = True
app.title = 'APP-19'

#---------------------------------------------------------------
#import data and cleaning 

client = MongoClient("mongodb+srv://Abdelk:lokmanezain@cluster0.8ploz.mongodb.net/covid_data?retryWrites=true&w=majority")
db = client.covid_data
collection_lok = db.lok
collection_SIR = db.SIR
df_total = pd.DataFrame(list(collection_SIR.find())).dropna()
df = pd.DataFrame(list(collection_lok.find()))

# drop _id to avoid losing columns
df_wt_id = df.drop('_id', axis =1)

# drop lines with NaN values

df_wt_NaN = df_wt_id.dropna()

df_france = df_wt_NaN.groupby('nom', as_index=False)[['hospitalises','reanimation','gueris','deces']].sum()
print (df_france[:5])

#----------------------------------------------------------------------------------------------------------------

dash_colors = {
    'background': '#111111',
    'text': '#BEBEBE',
    'grid': '#333333',
    'red': '#BF0000',
    'blue': '#466fc2',
    'green': '#5bc246'
}

#------------------------------------------------------------------
@app.callback(
    Output('hospit_ind', 'figure'),
    [Input('global_format', 'value')])
    
def confirmes(view):
	
    '''
    creation d'indicateurs des cas hospitalisées 
    '''
    

    value = df_total[df_total['date'] == df_total['date'].iloc[-1]]['casConfirmes'].sum()
    delta = df_total[df_total['date'] == df_total['date'].unique()[-2]]['casConfirmes'].sum()
    return {
            'data': [{'type': 'indicator',
                    'mode': 'number+delta',
                    'value': value,
                    'delta': {'reference': delta,
                              'valueformat': ',g',
                              'relative': False,
                              'increasing': {'color': dash_colors['blue']},
                              'decreasing': {'color': dash_colors['green']},
                              'font': {'size': 25}},
                    'number': {'valueformat': ',',
                              'font': {'size': 50}},
                    'domain': {'y': [0, 1], 'x': [0, 1]}}],
            'layout': go.Layout(
                title={'text': "cas Confirmes"},
                font=dict(color=dash_colors['red']),
                paper_bgcolor=dash_colors['background'],
                plot_bgcolor=dash_colors['background'],
                height=200
                )
            }



@app.callback(
    Output('recovered_ind', 'figure'),
    [Input('global_format', 'value')])
def recovered(view):
    '''
    creation des cas gueris
    '''
   

    value = df_total[df_total['date'] == df_total['date'].iloc[-1]]['gueris'].sum()
    delta = df_total[df_total['date'] == df_total['date'].unique()[-2]]['gueris'].sum()
    return {
            'data': [{'type': 'indicator',
                    'mode': 'number+delta',
                    'value': value,
                    'delta': {'reference': delta,
                              'valueformat': ',g',
                              'relative': False,
                              'increasing': {'color': dash_colors['blue']},
                              'decreasing': {'color': dash_colors['green']},
                              'font': {'size': 25}},
                    'number': {'valueformat': ',',
                              'font': {'size': 50}},
                    'domain': {'y': [0, 1], 'x': [0, 1]}}],
            'layout': go.Layout(
                title={'text': "Guéris"},
                font=dict(color=dash_colors['red']),
                paper_bgcolor=dash_colors['background'],
                plot_bgcolor=dash_colors['background'],
                height=200
                )
            }

@app.callback(
    Output('deaths_ind', 'figure'),
    [Input('global_format', 'value')])
def deaths(view):
    '''
    creation des cas décédées
    '''
    

    value = df_total[df_total['date'] == df_total['date'].iloc[-1]]['deces'].sum()
    delta = df_total[df_total['date'] == df_total['date'].unique()[-2]]['deces'].sum()
    return {
            'data': [{'type': 'indicator',
                    'mode': 'number+delta',
                    'value': value,
                    'delta': {'reference': delta,
                              'valueformat': ',g',
                              'relative': False,
                              'increasing': {'color': dash_colors['blue']},
                              'decreasing': {'color': dash_colors['green']},
                              'font': {'size': 25}},
                    'number': {'valueformat': ',',
                              'font': {'size': 50}},
                    'domain': {'y': [0, 1], 'x': [0, 1]}}],
            'layout': go.Layout(
                title={'text': "Décès hors Ehpad"},
                font=dict(color=dash_colors['red']),
                paper_bgcolor=dash_colors['background'],
                plot_bgcolor=dash_colors['background'],
                height=200
                )
            }
 


@app.callback(
    Output('france_trend', 'figure'),
    [Input('global_format', 'value'),
     Input('population_select', 'value')]
)
     
def france_trend(view, population):
    '''
    creation statistiques cumulées pour la visualization )
    '''
    
    if population == 'absolute':
        confirmes = df_total.groupby('date')['casConfirmes'].sum()
        gueris = df_total.groupby('date')['gueris'].sum()
        deces = df_total.groupby('date')['deces'].sum()
        title_suffix = ''
        hover = '%{y:,g}'
    # ~ elif population == 'percent':
        # ~ df = df.dropna(subset=['population'])
        # ~ hospitalises = df.groupby('date')['hospitalises'].sum() / df.groupby('date')['population'].sum()
        # ~ reanimation = df.groupby('date')['reanimation'].sum() / df.groupby('date')['population'].sum()
        # ~ gueris = df.groupby('date')[' gueris'].sum() / df.groupby('date')['population'].sum()
        # ~ deces =df.groupby('date')['deces'].sum() / df.groupby('date')['population'].sum()
        # ~ title_suffix = ' per 100,000 people'
        # ~ hover = '%{y:,.2f}'

    traces = [go.Scatter(
                    x=df.groupby('date')['date'].first(),
                    y=confirmes,
                    hovertemplate=hover,
                    name="Cas Confirmes",
                    mode='lines'),
                go.Scatter(
                    x=df.groupby('date')['date'].first(),
                    y=gueris,
                    hovertemplate=hover,
                    name="Guéris",
                    mode='lines'),
                go.Scatter(
                    x=df.groupby('date')['date'].first(),
                    y=deces,
                    hovertemplate=hover,
                    name="Décès hors Ehpad",
                    mode='lines')]
    return {
            'data': traces,
            'layout': go.Layout(
                title="{} Données Covid 19 {}".format(view, title_suffix),
                xaxis_title="Date",
                yaxis_title="Nombre de Cas",
                font=dict(color=dash_colors['text']),
                paper_bgcolor=dash_colors['background'],
                plot_bgcolor=dash_colors['background'],
                xaxis=dict(gridcolor=dash_colors['grid']),
                yaxis=dict(gridcolor=dash_colors['grid'])
                )
            }


#-----------------------------------------------------------------------------------------


app.layout = html.Div(style={'backgroundColor': dash_colors['background']}, children=[
    html.H1(
        children='Covid-19 DashBoard - France-',
        style={
            'textAlign': 'center',
            'color': dash_colors['text']
        }
    ),
    
    
        

    html.Div(dcc.RadioItems(id='global_format',
            
            value='France',
            labelStyle={'float': 'center', 'display': 'inline-block'}
            ), style={'textAlign': 'center',
                'color': dash_colors['text'],
                'width': '100%',
                'float': 'center',
                'display': 'inline-block'
            }
        ),


    html.Div(dcc.RadioItems(id='population_select',
            options=[{'label': 'Valeur totale', 'value': 'absolute'},
                        {'label': 'Valeur par 100,000 habitants', 'value': 'percent'}],
            value='absolute',
            labelStyle={'float': 'center', 'display': 'inline-block'},
            style={'textAlign': 'center',
                'color': dash_colors['text'],
                'width': '100%',
                'float': 'center',
                'display': 'inline-block'
                })
        ), 
        
        
     html.Div(
                dcc.Graph(id='france_trend'),
                style={'width': '50%', 'float': 'left', 'display': 'inline-block'}
                ),
    
    
    html.Div(dcc.Graph(id='hospit_ind'),
        style={
            'textAlign': 'center',
            'color': dash_colors['red'],
            'width': '25%',
            'float': 'left',
            'display': 'inline-block'
            }
        ),

    html.Div(dcc.Graph(id='deaths_ind'),
        style={
            'textAlign': 'center',
            'color': dash_colors['red'],
            'width': '25%',
            'float': 'left',
            'display': 'inline-block'
            }
        ),

    html.Div(dcc.Graph(id='recovered_ind'),
        style={
            'textAlign': 'center',
            'color': dash_colors['red'],
            'width': '25%',
            'float': 'left',
            'display': 'inline-block'
            }
        ), 
	
    html.Div(children='Données globales en France.', style={
        'textAlign': 'center',
        'color': dash_colors['text']
    }),
    
    
    
    
  
 
	html.Div([
			dash_table.DataTable(
				id='datatable_id',
				data=df_france.to_dict('records'),
				columns=[
					{"name": i, "id": i, "deletable": False, "selectable": False} for i in df_france.columns
				],
				editable=False,
				filter_action="native",
				sort_action="native",
				sort_mode="multi",
				row_selectable="multi",
				row_deletable=False,
				selected_rows=[],
				page_action="native",
				page_current= 0,
				page_size= 6,
				# page_action='none',
				# style_cell={
				# 'whiteSpace': 'normal'
				# },
				# fixed_rows={ 'headers': True, 'data': 0 },
				# virtualization=False,
				style_cell_conditional=[
					{'if': {'column_id': 'nom'},
					 'width': '20%', 'textAlign': 'left'},
					{'if': {'column_id': 'deces'},
					 'width': '20%', 'textAlign': 'left'},
					{'if': {'column_id': 'gueris'},
					 'width': '20%', 'textAlign': 'left'},
					 {'if': {'column_id': 'hospitalises'},
					 'width': '20%', 'textAlign': 'left'},
					 {'if': {'column_id': 'reanimation'},
					 'width': '20%', 'textAlign': 'left'}
				],
			),
    ],className='row'),

    html.Div([
        html.Div([
            dcc.Dropdown(id='linedropdown',
                options=[
                         {'label': 'deces', 'value': 'deces'},
                         {'label': 'gueris', 'value': 'gueris'},
                         {'label': 'hospitalises', 'value': 'hospitalises'},
                         {'label': 'reanimation', 'value': 'reanimation'}
                ],
                value='deces',
                multi=False,
                clearable=False
            ),
        ],className='six columns'),

        html.Div([
        dcc.Dropdown(id='piedropdown',
            options=[
                     {'label': 'deces', 'value': 'deces'},
                     {'label': 'gueris', 'value': 'gueris'},
                     {'label': 'hospitalises', 'value': 'hospitalises'},
                     {'label': 'reanimation', 'value': 'reanimation'}
                     
            ],
            value='gueris',
            multi=False,
            clearable=False
        ),
        ],className='six columns'),

    ],className='row'),

   
    
    html.Div([
        html.Div([
            dcc.Graph(id='linechart'),
        ],className='six columns'),

        html.Div([
            dcc.Graph(id='piechart'),
        ],className='six columns'),

    ],className='row'),
    
   
    html.Div(dcc.Markdown('''
            &nbsp;  
            &nbsp;  
            Construit par [Abdelkader Hamadi](https://www.linkedin.com/in/gregrafferty/)  
            Source des données: [COVID-19 - France](https://dashboard.covid19.data.gouv.fr/vue-d-ensemble?location=FRA)  
            
            '''),
            style={
                'textAlign': 'center',
                'color': dash_colors['text'],
                'width': '100%',
                'float': 'center',
                'display': 'inline-block'}
            )
    

]
)
@app.callback(
    [Output('piechart', 'figure'),
     Output('linechart', 'figure')],
    [Input('datatable_id', 'selected_rows'),
     Input('piedropdown', 'value'),
     Input('linedropdown', 'value')]
)
def update_data(chosen_rows,piedropval,linedropval):
    if len(chosen_rows)==0:
        df_filterd = df_france[df_france['nom'].isin(['Aude','Ariège','Aube', 'Aisne'])]
    else:
        print(chosen_rows)
        df_filterd = df_france[df_france.index.isin(chosen_rows)]

    pie_chart=px.pie(
            data_frame=df_filterd,
            names='nom',
            values=piedropval,
            hole=.3,
            labels={'nom':'Departement'}
            )


    #extract list of chosen departement
    list_chosen_countries=df_filterd['nom'].tolist()
    
    #filter original df according to chosen countries
    #because original df has all the complete dates
    df_line = df[df['nom'].isin(list_chosen_countries)]
    print(df_line[:5])

    line_chart = px.line(
            data_frame=df_line,
            x='date',
            y=linedropval,
            color='nom',
            labels={'nom':'Departements', 'date':'date'},
            )
    line_chart.update_layout(uirevision='foo')

    return (pie_chart,line_chart)


#------------------------------------------------------------------

if __name__ == '__main__':
    app.run_server(debug=False)
